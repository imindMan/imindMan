<div id="header" align="center"><img src="https://cdn.discordapp.com/attachments/1064529422079758346/1064534635838717972/unnamed.gif"/></div>

<p align="center">
  <a href="https://git.io/typing-svg"><img src="https://readme-typing-svg.demolab.com?font=Fira+Code&size=15&pause=1000&width=435&lines=CODING+EVERYDAY+AND+EVERYWHERE+LIKE+A+HACKER" alt="Typing SVG" /></a>
</p>

<div id="badges" align="center">
  <a href="https://github.com/imindMan">
    <img src="https://img.shields.io/badge/Github-blue?style=for-the-badge&logo=Github&logoColor=white" alt="Github Badge"/>
  </a>
  <a href="https://discordapp.com/users/917681283595919391">
    <img src="https://img.shields.io/badge/Discord-purple?style=for-the-badge&logo=Discord&logoColor=white" alt="Discord Badge"/>
  </a>
  <a href="https://open.spotify.com/user/otcorbsq9163zy1rq8apka7u7">
    <img src="https://img.shields.io/badge/Spotify-green?style=for-the-badge&logo=Spotify&logoColor=white" alt="Spotify Badge"/>
  </a>
  <a href="https://www.reddit.com/user/imindMan">
    <img src="https://img.shields.io/badge/Reddit-orange?style=for-the-badge&logo=Reddit&logoColor=white" alt="Reddit Badge"/>
  </a>
</div>
<div id="header" align="center">
  <img src="https://komarev.com/ghpvc/?username=imindMane&style=flat-square&color=blue" alt=""/>
  <img src="https://img.shields.io/github/stars/imindMan?affiliations=OWNER%2CCOLLABORATOR" alt=""/>
</div>

# A short introduction about me 👀

> Nothing special
>
> -imindMan

Somehow I'm not a newbie anymore. But, I'm still lol. 🤡

My real name is **Thái Lê Khánh Đông**. I'm from **Vietnam** 🇻🇳 and I'm currently a **student** 🏫. I'm **still learning** how to code like a pro 👩‍💻. 

I love **Linux** 🐧, and ***I use Arch Linux, btw***. But somehow you'll still meet me using **Microsoft products** 🪟, that's because I'm ~~**forced to use them.**~~

As expected, I'm another **alone coder** 💻. **~~I'm good at coding~~.** and I still **wanna reach to the expert level.**

# Something special about me 
As I said, nothing special.

Well, I'm **a student**, and currently **a solo coder** 💻. But I still **wanna contribute some free and open-source softwares** (because I'm a big fan of **FOSS** and **LINUS TORVALD** 🐧 🐧).

I **mainly code** in **Python 🐍, C/C++ 👴** and **currently learn how to code Rust, JavaScript and Lua 👶**.

I use **Neovim for mainly coding 📝, Git/GitHub for storing code online 🏪** and **literally everything on Linux 🤣**. Somehow I ~~am forced to use~~ use Microsoft products 🪟. 

# My achievements
🔥 🔥 🔥


[![Github Stats](https://github-readme-stats.vercel.app/api?username=imindMan&layout=compact&theme=nord&show-icons=true)](https://github.com/anuraghazra/github-readme-stats)
[![Top Langs](https://github-readme-stats.vercel.app/api/top-langs/?username=imindMan&layout=compact&theme=nord)](https://github.com/anuraghazra/github-readme-stats)

